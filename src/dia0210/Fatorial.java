package dia0210;

public class Fatorial {
	public static int fatorial (int x) throws InterruptedException{
		if(x<0){
			//throw new IllegalArgumentException("x tem valor inválido");
			throw new FatorialException(x);
		}
		int resp = 1;
		while(x>1){
			resp = resp*x;
			x--;
		}
		Thread.sleep(1);
		return resp;
	}
}
