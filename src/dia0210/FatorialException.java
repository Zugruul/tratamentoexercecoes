package dia0210;

public class FatorialException extends RuntimeException{
	private int valor;
	
	public FatorialException(int valor) { 
		super("Valor inválido para fatorial");
		this.valor = valor;
	}
	
	public int getValor(){
		return valor;
	}
}
