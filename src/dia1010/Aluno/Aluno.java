package dia1010.Aluno;

public class Aluno {
    private int matricula;
    private String nome;
    private int anoNascimento;

    public Aluno(int umaMatr,String umNome,int umAnoNasc){
        if(umaMatr <= 0){ //Programação defensiva, antecipação de erros
        	throw new IllegalArgumentException("Matrícula inválida " + umaMatr);
        }
    	if(umNome == null){
    		throw new IllegalArgumentException("Nome é nulo!");
    	}
    	if(/*umNome == ""*/umNome.equals("")){
    		throw new IllegalArgumentException("Nome está vázio!");
    	}
    	if(umAnoNasc < 1900){
    		throw new IllegalArgumentException("Ano inválido. Ano anterior à 1900: " + umAnoNasc);
    	}
    	nome = umNome;
        matricula = umaMatr;
        anoNascimento = umAnoNasc;
    }

    public int getMatricula() {  return matricula;   }

    public String getNome() { return nome;  }

    public int getAnoNascimento() {
        return anoNascimento;
    }
  
    @Override
    public String toString(){ 
       return(matricula+", "+nome+", "+anoNascimento);  
    }
}
