package dia1010.ContaCorrente;

public class SaldoInsuficienteException extends RuntimeException{ // excecões não verificadas são de RuntimeException
	
	private double valor;
	
	public SaldoInsuficienteException(double valor){
		super("Valor inválido: " + valor);
		this.valor = valor;
	}
	
	public double getValor(){
		return valor;
	}

}
