package dia1010.ContaCorrente;

public class ValorInvalidoException extends Exception{
	
	private double valor;
	
	public ValorInvalidoException(double valor){
		super("Saldo insuficiente: " + valor);
		this.valor = valor;
	}
	
	public double getValor(){
		return valor;
	}

}
