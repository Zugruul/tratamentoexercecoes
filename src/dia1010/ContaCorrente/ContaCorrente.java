package dia1010.ContaCorrente;

public class ContaCorrente {
    private double saldo;
    
    public ContaCorrente(double saldoInicial) throws ValorInvalidoException{
    	if(saldoInicial <= 0)
    		throw new ValorInvalidoException(saldoInicial);
        saldo = saldoInicial;
    }

    public void deposito(double valor) throws ValorInvalidoException{
    	if(valor <= 0)
    		throw new ValorInvalidoException(valor);
        saldo += valor;
    }

    public void retirada(double valor) throws ValorInvalidoException{
        if(valor <= 0)
        	throw new ValorInvalidoException(valor);
        if(valor > saldo)
        	throw new SaldoInsuficienteException(valor);
    	saldo -= valor;
    }

    public double getSaldo(){ 
        return(saldo);   
    }
}
