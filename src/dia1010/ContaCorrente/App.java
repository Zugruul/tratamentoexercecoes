package dia1010.ContaCorrente;

public class App {
	public static void main(String[] args){
		ContaCorrente c = null; // Inicializa fora 
		try { // é obrigado a verificar a excessão
			c = new ContaCorrente(100);
		} catch (ValorInvalidoException e) {
			System.out.println("Impossível criar conta.");
		}
		if(c != null)
			System.out.println("Saldo: " + c.getSaldo()); // Dá erro se iniciar conta corrente dentro do Try
		
		try {
			c.deposito(100);
			c.retirada(201); // se esquecer do try Catch e fizer uma excecao nao verificada dá erro
		} catch (ValorInvalidoException e) { // excessão verificada deve ser feita dentro de um bloco try catch
			System.out.println("Deposito não realizado!");
		//} catch (RuntimeException e){ // ele é superclasse de saldo insuficiente, se estiver antes nunca chegara o tratamento da excecao SaldoInsuficienteException
		//	System.out.println("Erro na execução!");
		} catch (SaldoInsuficienteException e) {//adicionando esse catch ao dar erro em retirada algo acontece //
			//e.printStackTrace();
			//e.getMessage();
			//throw e; //Não é comigo, passar pra quem vai tratar!
			System.out.println("Retirada não realizada!");
		}	
		// Até aqui exercício 2
			
		// Exercício 3 - Ver classe exception na documentação do java para ver tipos de excecoes (Não verificadas abaixo de RuntimeException)
		// Runtime, excessões não verificadas: Nullpointer, Arithmetic
		
		// Exercício 4 e 5 - Diferença entre verificada e não.
			//Nullpointer é derivada de Runtime, ou seja, não é verificada. Normalmente ocorre por erro de programação.
		// Exerc 6 - O programa estoura.
		// Exerc 7 - O que pode fazer com a excessão (o 'e') - pode-se usar os métodos dele
	}
}
